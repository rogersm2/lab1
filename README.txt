1.    List the common reasons for making a branch in the repository.
	
	A useful reason to use a branch could be for hotfix's, so that a fix can be fully 		tested and then merged back to the production build with the fix. They are also 		useful for building programs, when a new idea is or direction is taken sometimes it 	does not work well further down the road. Luckily a person can backtrack out of all 	unwanted code to the last branch.


2.    For each of these reasons, briefly discuss whether you would normally expect such a         branch to cause a conflict when merged and why.
	
	In the case of a hotfix I dont think there is a large chance of conflict since the 		programmer would be working with the current production system and anything added 		would be tested before merged. When merging a long branch with many commits there 		would definately be a chance of conflict because sometimes existing code must be 		changed in order to fuction better with new code that is added.


3.    Describe what happened when you attempted to merge branch B into master above. 

	GiT realized that I was committing two files that had the same name/location but 		were not identical. This showed that there was a descrepancy that needed to be 			resolved in case it hurts the system.


4.    Describe what you did to resolve the problem and complete the merge.

	I just realized I did my problem in a differnt way. I should have had two of the 		same text files with different writing/data in them and caused the problem that 		way. For some reason I only renamed a file in the master that was different from B, 	and when I merged it GiT did notice that master had a differnt file than that of B. 	But instead of making me fix it, it just updated to how master was by itself. So I 		did learn something about that at least, and I do know that if I had caused the 		problem right I would have had to update the text file then commit it.


5.    Describe what you had to do in the last two steps to successfully merge          O�s changes into F and push from F to the remote.

	I was not able to work this problem exactly like you did, interestingly 	I somehow managed to link the lab1 remote repositories to both the O and 	F repositories. It actually gave me a new master that is called ORIGIN 	MASTER. And it seems to update both of them as I push and I cannot get 	it to through me a problem.
	
	If I had to guess I would imagine I would have to update the O file to 		match the F and push that.